# program to add variable length integer arguments passed to the function.
# [Also demo the use of docstrings]
def demo(*values):
	'''Variable length arguments.'''
	s=0
	for i in values:
		s=s+i
	print("sum is",s)

print(demo.__doc__)
demo(2,3,5,8)

