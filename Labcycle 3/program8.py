# program to sum the series 1/1! + 4/2! + 27/3! + ..... + nth term. [ Hint :
# Use function to find the factorial of a number].
def fact(n):
	if n==1:
		return 1
	else:
		return n*fact(n-1)
n=int(input("Enter the number of terms: "))
result=0
for i in range(1,n+1):
	f=fact(i)
	result=result+(pow(i,i)/f)
print("sum of series is ",result)
