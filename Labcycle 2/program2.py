# Program That accepts a string from the user and redisplays 
#the string after removing vowels from it

text=input("Enter the string: ")
vowels=['a','e','i','o','u','A','E','I','O','U']
newtext=" "
for i in text:
    if i not in vowels:
        newtext=newtext+i
print("\n With vowels :",text)
print("\n Without Vowels :",newtext)