# Program to determine whether the character entered is a vowel or not

# Get a character from the user
character=input("enter a leter ")

# Create a list containing vowels
list=["a","e","i","o","u","A","E","I","O","U"]

# check whether the character in the list
if(character in list):
  # If yes,print the character is a vowel 
  print("{0} is a vowel".format(character))
else:
  # If not, print the character is not a vowel 
  print("{0} is not a vowel".format(character))   