# program to print the reverse of a number. [use while loop]
# Get the number from the user
n=int(input("Enter a number:"))
x=n
rev=0
# Loop to find the reverse
while(n>0):
 rem=int(n%10)
 rev=int(rev*10+rem)
 n=int(n/10)
# Print the reverse of the number  
print("Reverse of {0} is {1} ".format(x,rev))