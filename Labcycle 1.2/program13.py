#Program to print the sum of series-1/2 + 2/3+....

# Get the limit from the user
n=int(input("Enter the limit:"))
sum1=0
# Loop to print the sum
for i in range (1,n+1):
    sum1=sum1+(i/(i+1))
    print(i,"/",i+1,end="")
    if(i!=n):
      print(" + ", end="")
    else:  
      print()
print("sum=", sum1)      
      