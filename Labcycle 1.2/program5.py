# program to find the sum of all items in a list.
# Create a list containing numbers
list=[1,6,8,5]

#Get the length of the list
n=len(list)
print(list) # Print the list
sum=0 

#Loop through the list
for i in range(0, n):
   #calculate the sum of the values in the list
   sum=int(sum)+int(list[i])
print("sum=",sum) #print the sum
