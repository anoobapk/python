# Program to generate all factors of a number. [use while loop]
# Get a number from the user
a=int(input("Enter a number "))
i=1 
# loop to print the factors
while (i<=a):
 #check whether the number is divisible 
 if (a%i==0):
    #if yes, print the divisor 
    print(" ",i)
 #Increment the value of i
 i=i+1