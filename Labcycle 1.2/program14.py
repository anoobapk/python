#consruct pattern using nested loop

# Get the limit from the user
n=int(input("Enter limit:"))
#  For loop to print the pattern
for i in range(0, n):
    for j in range(0, i + 1):
        print("*", end=' ')
    print()

for i in range(n, 0, -1):
    for j in range(0, i - 1):
        print("*", end=' ')
    print()