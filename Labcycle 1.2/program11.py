#Program to find the average of numbers\

# Get the limit from the user
n=int(input("Enter the limit: "))
sum=0
# Loop to find the sum
for i in range(1,n+1):
    sum=sum+i
# Calculate the average     
average=sum/n
# Print the average
print("sum= ",sum)
print("Average=",average)