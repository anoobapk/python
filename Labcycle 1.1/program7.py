# program to read a start and end year from the user and display all
# leap years between them.
# Get first year from the user
a=int(input("enter first year "))

#Get last year from the user
b=int(input("enter last year "))
print("leap years are:")

#Loop through the years
for i in range(a, b+1):
 # check whether the year is divisible by 4
 if (i%4==0):
   {   # if yes, print year
       print(i)
   }
else:
 #  check whether the year is divisible by 400
  if (i%400==0):
        #if yes print year
         print(i)

