# Program to calculate the area of a circle by reading inputs from the user
#create constat value pi
pi=3.14

#get radius from the user
r=int(input("enter radius of the circle: "))

#claculate area of the circle 
area=float(pi*r*r)

#print the area
print("The are of the circle with radius {0} ={1}".format(r,area))