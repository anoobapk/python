# Program to perform arithmetic operations on two integer numbers.
# Get two number from the user
a=int(input("enter a number "))
b=int(input("enter a number "))

#print the result of arithmetic operations
print(" {0}+{1} = {2}".format(a,b,a+b))
print(" {0}-{1} = {2}".format(a,b,a-b))
print(" {0}*{1} = {2}".format(a,b,a*b))
print(" {0}/{1} = {2}".format(a,b,a/b))
print(" {0}%{1} = {2}".format(a,b,a%b))